$(function () {

	var body = $('body'),
		stage = $('#stage'),
		back = $('a.back');

	/* Step 1 */

	$('#step1 .encrypt').click(function () {
		body.attr('class', 'encrypt');

		// Go to step 2
		step(2);
	});

	$('#step1 .decrypt').click(function () {
		body.attr('class', 'decrypt');
		step(2);
	});


	/* Step 2 */

	// Set up events for the file inputs

	$('#encrypt-input-file-btn').click(function () {
		$('#encrypt-input-file').click();
	})
	$('#encrypt-input-files-btn').click(function () {
		$('#encrypt-input-files').click();
	})
	$('#decrypt-input-file-btn').click(function () {
		$('#decrypt-input-file').click();
	})

	var files = null;

	$('#encrypt-input-files,#encrypt-input-file,#decrypt-input-file').on('change', function (e) {

		Array.from(e.target.files).forEach(f => {

			if (f.size > 1024 * 1024) {
				alert('Please choose files smaller than 1mb, otherwise you may crash your browser. \nThis is a known issue. See the tutorial.');
				return;
			} else {
				step(3);
			}

		});
		files = e.target.files;
	});


	/* Step 3 */


	$('a.button.process').click(function () {

		var input = $(this).parent().find('input[type=password]'),
			// a = $('#step4 a.download'),
			password = input.val();

		input.val('');

		// The HTML5 FileReader object will allow us to read the 
		// contents of the	selected file.

		if (body.hasClass('encrypt')) {

			// Encrypt the file!

			if ($(this).hasClass('aes')) {
				Array.from(files).forEach(f => {
					var reader = new FileReader();
					reader.onload = function (e) {

						// Use the CryptoJS library and the AES cypher to encrypt the 
						// contents of the file, held in e.target.result, with the password

						var encrypted = CryptoJS.AES.encrypt(e.target.result, password);

						// The download attribute will cause the contents of the href
						// attribute to be downloaded when clicked. The download attribute
						// also holds the name of the file that is offered for download.

						var a = $('<a>').addClass('button download green').html('<i class="fas fa-gifts"></i>');
						a.attr('href', 'data:application/octet-stream,' + encrypted);
						a.attr('download', f.name + '_AES.encrypted');
						a.appendTo('#content_result');
					};

					// This will encode the contents of the file into a data-uri.
					// It will trigger the onload handler above, with the result
					reader.readAsDataURL(f);
				});
				step(4);
			}

			if ($(this).hasClass('des')) {
				Array.from(files).forEach(f => {
					var reader = new FileReader();
					reader.onload = function (e) {

						// Use the CryptoJS library and the AES cypher to encrypt the 
						// contents of the file, held in e.target.result, with the password

						var encrypted = CryptoJS.DES.encrypt(e.target.result, password);

						// The download attribute will cause the contents of the href
						// attribute to be downloaded when clicked. The download attribute
						// also holds the name of the file that is offered for download.

						var a = $('<a>').addClass('button download green').html('<i class="fas fa-gifts"></i>');
						a.attr('href', 'data:application/octet-stream,' + encrypted);
						a.attr('download', f.name + '_DES.encrypted');
						a.appendTo('#content_result');
					};

					// This will encode the contents of the file into a data-uri.
					// It will trigger the onload handler above, with the result
					
					reader.readAsDataURL(f);
				});
				step(4);
			}
		}
		else {

			// Decrypt it!

			if ($(this).hasClass('aes')) {
				Array.from(files).forEach(f => {
					var reader = new FileReader();
					reader.onload = function (e) {

						var decrypted = CryptoJS.AES.decrypt(e.target.result, password)
							.toString(CryptoJS.enc.Latin1);

						if (!/^data:/.test(decrypted)) {
							alert("Invalid pass phrase or file! Please try again.");
							return false;
						} else {
							var a = $('<a>').addClass('button download green').html('<i class="fas fa-gifts"></i>');
							a.attr('href', decrypted);
							a.attr('download', f.name.replace('_AES.encrypted', ''));
							a.appendTo('#content_result');
							step(4);
						}
					}
					reader.readAsText(f);
				});
			};
			if ($(this).hasClass('des')) {
				Array.from(files).forEach(f => {
					var reader = new FileReader();
					reader.onload = function (e) {

						var decrypted = CryptoJS.DES.decrypt(e.target.result, password)
							.toString(CryptoJS.enc.Latin1);

						if (!/^data:/.test(decrypted)) {
							alert("Invalid pass phrase or file! Please try again.");
							return false;
						} else {
							var a = $('<a>').addClass('button download green').html('<i class="fas fa-gifts"></i>');
							a.attr('href', decrypted);
							a.attr('download', f.name.replace('_DES.encrypted', ''));
							a.appendTo('#content_result');
							step(4);
						}
					}
					reader.readAsText(f);
				});
			}
		}
	});


	/* The back button */


	back.click(function () {

		$('.download').remove();

		step(1);
	});


	// Helper function that moves the viewport to the correct step div

	function step(i) {

		if (i == 1) {
			back.fadeOut();

		}
		else {
			back.fadeIn();
		}


		// Move the #stage div. Changing the top property will trigger
		// a css transition on the element. i-1 because we want the
		// steps to start from 1:

		stage.css('top', (-(i - 1) * 100) + '%');
	}

});
